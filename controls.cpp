/* Macros */ //kernel32.lib
/*
#ifndef UNICODE
    #define UNICODE
#endif
*/

/* Own macros */
#define PUBLIC
#define BUFFSZ 128
#define PRIVATE static
#define PERROR(text, caption, type, _bool) {\
	MessageBox(NULL, text, caption, type);\
	printf2(_bool, "RegisterClass(): %d", GetLastError());\
}

/* Includes */
#include <stdio.h>
#include <stdarg.h>
#include <windows.h>

/* Own includes */

/* Glocals */
struct _btn {
	int btnStyle;
	LPCTSTR btnText;
} BTNS[] = {
	BS_PUSHBUTTON,"PushBtn",
	BS_DEFPUSHBUTTON, "DefPushBtn",
	BS_CHECKBOX, "CheckBox",
	BS_AUTOCHECKBOX, "AutoCheckBox",
	BS_3STATE, "CB_3-State",
	BS_AUTO3STATE, "CB3-Cycle",
	BS_RADIOBUTTON, "RadioBtn",
	BS_AUTORADIOBUTTON, "AutoRadioBtn"
};
PRIVATE LPCTSTR mbName = "MessageBox_0xA0";
PRIVATE const char Path[] = "C:/Users/julio.garroz.riveros/codigo/c_c++/stdout";

/* Prototypes */
void WINAPI printf2(int append, const char *format, ...);
LRESULT CALLBACK wProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

/* Definitions */
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE, PWSTR argv, int nCmdShow)
{
	MSG msg;
	HWND hWin;
	WNDCLASS wc;
	char buff[BUFFSZ];
	LPCTSTR cName = "Class_0xA0";
	LPCTSTR wName = "Window_0xA0";

	wc = {};
	msg = {};
	memset(buff, 0, BUFFSZ);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = wProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_SIZENS);
	wc.hbrBackground = CreateSolidBrush((COLORREF)0x777777);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = cName;
	
	if (!RegisterClass(&wc)) {
		PERROR("F: RegisterClass()", wc.lpszClassName, MB_OK, false);
		return -1;
	}
	
	hWin = CreateWindow(cName, wName, WS_OVERLAPPEDWINDOW | WS_VISIBLE,
		CW_USEDEFAULT, CW_USEDEFAULT,
		CW_USEDEFAULT, CW_USEDEFAULT,
		NULL, NULL, hInstance, NULL);
	if (hWin == NULL) {
		PERROR("F: CreateWindow()", wc.lpszClassName, MB_OK, true);
		return -2;
	}
	while (GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return 0;
}


/*


				IMPORTANTE
lParam ---> Child window handle
LOWORD (wParam) ---> Child window ID
HIWORD (wParam) ---> Notification code



*/
LRESULT CALLBACK wProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	int i;
	HDC hdc;
	PAINTSTRUCT ps;
	PRIVATE RECT rect;
	PRIVATE HWND hwndArr[2];
	PRIVATE char szBuff[50];
	PRIVATE LPCTSTR szTop = "Message          wParam          lParam",
					szUnd = "_______          ______          ______",
					szFmt = "%-16s%04x-%04x          %04x-%04x";
	PRIVATE int cxChar, cyChar;

    switch (msg)
    {
		case WM_CREATE:
			cxChar = LOWORD(GetDialogBaseUnits());
			cyChar = HIWORD(GetDialogBaseUnits());
			
			/* First btn */
			hwndArr[0] = CreateWindow("button", BTNS[0].btnText,
				WS_CHILD | WS_VISIBLE | BTNS[0].btnStyle,
				cxChar, cyChar,20 * cxChar, 7 * cyChar / 4,
				hwnd, (HMENU) 0,
				((LPCREATESTRUCT)lParam)->hInstance, NULL);

			/* Second btn */
			hwndArr[1] = CreateWindow("button", BTNS[2].btnText,
				WS_CHILD | WS_VISIBLE | BTNS[2].btnStyle,
				cxChar, cyChar * (1 + 2 * 1), 20 * cxChar, 7 * cyChar / 4,
				hwnd, (HMENU)1,
				((LPCREATESTRUCT)lParam)->hInstance, NULL);

			/* Third btn */
			hwndArr[2] = CreateWindow("button", BTNS[6].btnText,
				WS_CHILD | WS_VISIBLE | BTNS[6].btnStyle,
				cxChar, cyChar * (1 + 2 * 2), 20 * cxChar, 7 * cyChar / 4,
				hwnd, (HMENU)2,
				((LPCREATESTRUCT)lParam)->hInstance, NULL);
			break;
		
		case WM_PAINT:
			InvalidateRect(hwnd, &rect, TRUE);
			hdc = BeginPaint(hwnd, &ps);
			SelectObject(hdc, GetStockObject(SYSTEM_FIXED_FONT));
			SetBkMode(hdc, TRANSPARENT);
			TextOut(hdc, 24 * cxChar, cyChar, szTop, lstrlen(szTop));
			TextOut(hdc, 24 * cxChar, cyChar, szUnd, lstrlen(szUnd));
			EndPaint(hwnd, &ps);
			//MessageBox(NULL, "WM_PAINT", mbName, MB_OK);
			return 0;

        case WM_CLOSE:
            DestroyWindow(hwnd);
            break;

        case WM_DESTROY:
            PostQuitMessage(0);
			break;

		case WM_SIZE:
			rect.bottom = HIWORD(lParam);
			rect.top = 2 * cyChar;
			rect.left = 24 * cxChar;
			rect.right = LOWORD(lParam);
			return 0;

		case WM_DRAWITEM:
		case WM_COMMAND:
			/* Setting msg in buffer */
			hdc = GetDC(hwnd);
			ScrollWindow(hwnd, 0, -cyChar, &rect, &rect);
			wsprintf(szBuff, szFmt, TEXT("WM_CMD+"),
				HIWORD(wParam), LOWORD(wParam), HIWORD(lParam), LOWORD(lParam));
			TextOut(hdc, (24 * cxChar), (cyChar * (rect.bottom /cyChar - 1)),
					szBuff,lstrlen(szBuff));
			ReleaseDC(hwnd, hdc);
			ValidateRect(hwnd, &rect);
			break;

        default:
            return DefWindowProc(hwnd,msg,wParam,lParam);
    }
    return TRUE;
}

void WINAPI printf2(int append, const char *format, ...)
{
	FILE *file;
	va_list argptr;
	char str[1024];

	if (append)
		file = fopen(Path, "a");
	else
		file = fopen(Path, "w");
	va_start(argptr, format);
	vfprintf(file, format, argptr);
	va_end(argptr);
	fclose(file);
}


/*
WCHAR -> typedef wchar_t WCHAR;
PWCHAR -> typedef WCHAR *PWCHAR;

LPCSTR -> typedef __nullterminated CONST CHAR *LPCSTR;
LPCTSTR -> #ifdef UNICODE ? typedef LPCWSTR LPCTSTR : typedef LPCSTR LPCTSTR;

LPCWSTR -> typedef CONST WCHAR *LPCWSTR;
LPWSTR -> typedef WCHAR *LPWSTR;
PWSTR -> typedef WCHAR *PWSTR;
*/

/*
LOWORD(wParam) -> child wind
HIWORD(wParam) -> Notification code


Notification codes:
BN_CLICKED			0
BN_PAINT			1
BN_HILITE			2
BN_PUSH				2
BN_UNHILITE			3
BN_UNPUSHED			3
BN_DISABLE			4
BN_DOUBLECLICKED	5
BN_DBLCLK			5
BN_SETFOCUS			6
BN_KILLFOCUS		7
*/