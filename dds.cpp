/* Includes */
#include "pch.h"
#include <stdio.h>
#include <string.h>
#include <Windows.h>
#include <Windows.h>

/* Imports */
//import std;

/* C++ style includes */
#include <list>
#include <iostream>

/* Macros */
#define SUCCESS 0
#define FAILURE -1
#define BUFSZ 128
#define CHBUFSZ 64
#define PRIVATE static

/* Glocals */
std::list<HWND> Btns;
PRIVATE int Iterator = 0;
PRIVATE LPCTSTR TargetName = L"Instalación de Mozilla Thunderbird";

/* Enums & Structs */
enum ERRORS {
	ERR_NAMENOTFOUND = 0xA0,
	ERR_MEMALLOC = 0xB0
};

enum C_CLASSES {
	CL_NONE = 0xC0,
	CL_BUTTON = 0xC1,
	CL_STATIC = 0xC2
};

/* Prototypes */
void __Abort__(int, bool);
const char* ErrPrint(int err);
void __Abort__(int err, bool act);
PRIVATE BOOL CALLBACK _GetChilds(HWND, LPARAM);
PRIVATE int GetChilds(HWND, HWND*, int);
void Action(HWND, HWND*, int);

/* Private prototypes */
int GetControlsByType_(HWND);
void GetControlsByType(HWND*, int);

/* Prototypes control */
void btnSetState(HWND, BOOL);
void checBoxSetState(HWND, BOOL);
WCHAR *btnGetTexxt(HWND, void*, int);

/* Definitions */
int main()
{
	int ctlSz;
	HWND target;
	HWND controls[CHBUFSZ];
	
	target = FindWindow(NULL, TargetName);
	if (target == NULL)
		__Abort__(ERR_NAMENOTFOUND, true);
	ctlSz = GetChilds(target, controls, CHBUFSZ);
	Action(target, controls, ctlSz);
	return 0;
}

void Action(HWND parent, HWND *controls, int ctlSz)
{ //000A048A
	HWND sig, aux;
	WCHAR buff[BUFSZ];

	GetControlsByType(controls, ctlSz);
	auto lFront = Btns.begin();
	std::advance(lFront, 1);
	sig = *lFront;
	printf("Next: %p\n", sig);
	aux = SetActiveWindow(parent);
	printf("AUX: %p. Error Code: %d\n", aux, GetLastError());
	SendMessage(sig, BM_CLICK, 0, 0);
	SendMessage(sig, BM_CLICK, 0, 0);
	/*for (std::list<HWND>::iterator it = Btns.begin(); it != Btns.end(); it++)
		wprintf(L"CL_BTN: %p (%s)\n", *it, btnGetTexxt(*it, (void *)buff, BUFSZ));*/
}

void GetControlsByType(HWND *controls, int cSz)
{
	int ctlType, cl_static, cl_other;

	cl_static = cl_other = 0;
	if (controls == NULL)
		return;

	for (int i = 0; i < cSz; i++) {
		ctlType = GetControlsByType_(controls[i]);
		switch (ctlType) {
		
		case CL_BUTTON:
			Btns.push_back(controls[i]);
			break;
		
		case CL_STATIC:
			cl_static++;
			break;

		default:
			cl_other++;
		}
	}
	printf("CL_STATIC: %d\n", cl_static);
	printf("CL_OTHER: %d\n", cl_other);
}

int GetControlsByType_(HWND control)
{
	int ret;
	WCHAR buff[BUFSZ];
	
	ret = CL_NONE;
	memset(buff, 0, BUFSZ);
	GetClassName(control, buff, BUFSZ);

	if (!_wcsicmp(buff, L"button"))
		ret = CL_BUTTON;
	else if (!_wcsicmp(buff, L"static"))
		ret = CL_STATIC;
	return (ret);
}

/* **************************************************************************************************** */
PRIVATE int GetChilds(HWND parent, HWND *buff, int buffSz)
{
	/* Get all existing controls for a given parent */
	memset(buff, 0, buffSz);
	EnumChildWindows(parent, _GetChilds, ((LPARAM)(void *)buff));
	return Iterator;
}

PRIVATE BOOL CALLBACK _GetChilds(HWND cHwnd, LPARAM lParam)
{
	/*
	 * Callback for EnumChildWindows
	 * cHwnd: Handler (HWND) for the current child
	 */
	HWND *childs;

	childs = (HWND *)((void*)lParam);
	if (childs != NULL)
		childs[Iterator++] = cHwnd;
	return TRUE;
}

void btnSetState(HWND button, BOOL press)
{
	/*
			Press/Depressed a button with the only purpose
			of demonstrate the use of GETSTATE/SETSTATE
	*/
	bool status;

	/*  Check the button state: true = pressed  */
	status = SendMessage(button, BM_GETSTATE, FALSE, 0);
	if (press) {
		if (!status)
			SendMessage(button, BM_SETSTATE, TRUE, 0);
	} else {
		/* Undepressed it */
		if (status)
			SendMessage(button, BM_SETSTATE, 0, 0);
	}
}

void checBoxSetState(HWND checkBox, BOOL check)
{
	/*
			Check/Uncheck a checkbox with the only purpose
			of demonstrate the use of GETCHECK/SETCHECK
	*/
	bool status;

	/*  Check the box state: true = checked  */
	status = SendMessage(checkBox, BM_GETSTATE, 0, 0);
	if (check) {
		if (!status)
			SendMessage(checkBox, BM_SETCHECK, BST_CHECKED, 0);
	}
	else {
		/* Uncheck it */
		if (status)
			SendMessage(checkBox, BM_SETCHECK, BST_UNCHECKED, 0);
	}
}

WCHAR *btnGetTexxt(HWND window, void *buff, int buffSz)
{
	if (window == NULL || buff == NULL)
		return (NULL);

	SendMessage(window, WM_GETTEXT, (WPARAM)buffSz, (LPARAM)buff);
	return ((WCHAR *)buff);
}

void __Abort__(int err, bool act)
{
	printf("%s\n", ErrPrint(err));
	if (act)
		_exit(FAILURE);
}

const char* ErrPrint(int err)
{
	/* Print a given error */
	switch (err) {
	case 0xA0:
		return ("Window name not found");
	case 0xB0:
		return ("!malloc (really weird error)");
	default:
		return ("");
	}
}