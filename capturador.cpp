/* Macros */

/* Includes */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>

/* Enums & Structs */

/* Glocals */
static int IT = 0;
const char W[] = "Window_0xA0";

/* Prototypes */
BOOL CALLBACK cbk(HWND hwnd, LPARAM lParam);
void btnSetText(HWND window, LPCTSTR msg);
void btnSetState(HWND window, BOOL depressed);
void checBoxSetState(HWND checkBox, BOOL check);

void btnGetTexxt(HWND window, void *buff, int buffSz);

/* Definitions */
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE, PWSTR argv, int nCmdShow)
{
	char B[50];
	WPARAM wParam;
	LPARAM lParam;
	HWND parent, childs[3];
	const char msg1[] = "Tomas";
	const char msg2[] = "Milena";
	const char msg3[] = "Julio";

	memset(childs, 0, sizeof(childs));
	parent = NULL;
	parent = FindWindowA(NULL, W);

	if (parent == NULL) {
		MessageBox(NULL, "Fallo", "Title TK", MB_OK);
		return -1;
	}

	EnumChildWindows(parent, cbk, (LPARAM)((void *)&childs));
	//sprintf(B, "%p %d", childs[2],SendMessage(childs[2],BM_GETCHECK,0,0));
	
	/* 1 */
	btnSetState(childs[0], true);
	Sleep(150);
	btnSetState(childs[0], false);
	
	/* 2 */
	Sleep(500);
	checBoxSetState(childs[1], true);
	Sleep(350);
	checBoxSetState(childs[1], false);

	/* 3 */
	Sleep(500);
	checBoxSetState(childs[2], true);
	Sleep(350);
	checBoxSetState(childs[2], false);

	/* 4 */
	Sleep(500);
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 2; j++) {
			SendMessage(childs[i], BM_CLICK, 0, 0);
			Sleep(200);
		}
		Sleep(200);
	}

	/* 5 */
	Sleep(500);
	btnSetText(childs[0], msg1);
	Sleep(300);
	btnSetText(childs[1], msg2);
	Sleep(300);
	btnSetText(childs[2], msg3);
	//SendMessage(childs[2], BM_SETCHECK, BST_INDETERMINATE, 0);
	//btnGetTexxt(childs[0], (void *)B, sizeof(B));
	//MessageBox(NULL, B, "Tittle TK", MB_OK);
	return 0;
}

void btnSetState(HWND button, BOOL press)
{
	/* 
		Press/Depressed a button with the only purpose
		of demonstrate the use of GETSTATE/SETSTATE
	*/
	bool status;

	/*  Check the button state: true = pressed  */
	status = SendMessage(button, BM_GETSTATE, FALSE, 0);
	if (press) {
		if (!status)
			SendMessage(button, BM_SETSTATE, TRUE, 0);
	} else {
		/* Undepressed it */
		if (status)
			SendMessage(button, BM_SETSTATE, 0, 0);
	}
}

void checBoxSetState(HWND checkBox, BOOL check)
{
	/*
		Check/Uncheck a checkbox with the only purpose
		of demonstrate the use of GETCHECK/SETCHECK
	*/
	bool status;

	/*  Check the box state: true = checked  */
	status = SendMessage(checkBox, BM_GETSTATE, 0, 0);
	if (check) {
		if (!status)
			SendMessage(checkBox, BM_SETCHECK, BST_CHECKED, 0);
	}
	else {
		/* Uncheck it */
		if (status)
			SendMessage(checkBox, BM_SETCHECK, BST_UNCHECKED, 0);
	}
}

void btnSetText(HWND window, LPCTSTR msg)
{
	SendMessage(window, WM_SETTEXT, 0, (LPARAM)msg);
}

void btnGetTexxt(HWND window, void *buff, int buffSz)
{
	if (window == NULL || buff == NULL)
		return;
	SendMessage(window, WM_GETTEXT, (WPARAM)buffSz, (LPARAM)buff);
}

BOOL CALLBACK cbk(HWND cHwnd, LPARAM lParam)
{
	char B[50];
	HWND *childs;

	/* Getting (also) the ID of the window*/
	//int id = GetWindowLong(cHwnd, GWL_ID);
	int childID = GetDlgCtrlID(cHwnd);
	childs = (HWND *)((void* )lParam);
	childs[IT++] = cHwnd;
	//sprintf(B, "child: %p\n", cHwnd);
	//MessageBox(NULL, B, "Ttitle", MB_OK);
	return TRUE;
}

//Wh@t1H@pp#n#d7T+0FF9