/* Macros */
#define BUFFSZ 128

/* Includes */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>

/* Glocals */
const char Path[] = "C:/Users/julio.garroz.riveros/codigo/c_c++/stdout";

/* Prototypes */
void _print();
void _compile();

/* Definitions */
int main(int argc, char **argv)
{
	char cmd;

	cmd = 'C';
	if (argc > 1)
		cmd = argv[1][0];
	
	switch (cmd) {
	case 's':
	case 'S':
		system("cl cptc\\capturador.cpp user32.lib gdi32.lib /link /out:cp.exe");
		system("del *.obj");
		break;
	
	case 'r':
	case 'R':
		if (cmd == 'R')
			_compile();
		system("cptc\\controls.exe");
		//system("cls");
		break;

	case 'p':
	case 'P':
		_print();
		break;

	case 'c':
	case 'C':
		_compile();
		break;

	default:
		return 0;
	}
	return 0;
}

void _compile()
{
	system("cl cptc\\controls.cpp user32.lib gdi32.lib /link /out:cptc\\controls.exe");
	system("del *.obj");
}

void _print()
{
	FILE *f;
	unsigned char buff[BUFFSZ];

	memset(buff, 0, BUFFSZ);
	f = fopen(Path, "r");
	if (f == NULL)
		return;
	fread((void *)buff, BUFFSZ, 1, f);
	if (!ferror(f))
		printf("%s\n", buff);
	fclose(f);
}